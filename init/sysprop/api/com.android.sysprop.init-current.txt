props {
  module: "android.sysprop.InitProperties"
  prop {
    api_name: "userspace_reboot_in_progress"
    access: ReadWrite
    prop_name: "sys.init.userspace_reboot.in_progress"
    integer_as_bool: true
  }
}
